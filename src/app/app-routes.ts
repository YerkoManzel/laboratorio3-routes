import {Routes} from '@angular/router';
import {DashboardNotFoundComponent} from './dashboard/dashboard-not-found/dashboard-not-found.component';
import {CustomGuard} from './custom/custom.guard';

export const APP_ROUTES_CONFIG: Routes = [
  {
    path: 'custom',
    loadChildren: './custom/custom.module#CustomModule'
  },
  {
    path: 'ng-bootstrap',
    loadChildren: './ng-bootstrap/ng-bootstrap.module#NgBootstrapModule',
    canLoad: [CustomGuard]
  },
  {
    path: 'primeng',
    loadChildren: './prime-ng/prime-ng.module#PrimeNGModule'
  },
  {
    path: '',
    redirectTo: '/custom',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: DashboardNotFoundComponent
  }
];
