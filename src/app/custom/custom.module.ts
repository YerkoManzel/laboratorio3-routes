import {NgModule} from '@angular/core';
import {CustomFirstComponent} from './custom-first/custom-first.component';
import {CustomSecondComponent} from './custom-second/custom-second.component';
import {CustomMainComponent} from './custom-main/custom-main.component';
import {CustomRoutingModule} from './custom-routing.module';
import {CustomDetailsComponent} from './custom-details/custom-details.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  declarations: [CustomFirstComponent, CustomSecondComponent, CustomMainComponent, CustomDetailsComponent],
  imports: [
    SharedModule,
    CustomRoutingModule
  ]
})
export class CustomModule {
}
