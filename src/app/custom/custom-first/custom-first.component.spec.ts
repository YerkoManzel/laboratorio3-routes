import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomFirstComponent } from './custom-first.component';

describe('CustomFirstComponent', () => {
  let component: CustomFirstComponent;
  let fixture: ComponentFixture<CustomFirstComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomFirstComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomFirstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
