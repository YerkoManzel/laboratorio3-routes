import {NgModule} from '@angular/core';

import {NgBootstrapRoutingModule} from './ng-bootstrap-routing.module';
import {NgBootstrapMainComponent} from './ng-bootstrap-main/ng-bootstrap-main.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgBootstrapButtonsComponent} from './ng-bootstrap-buttons/ng-bootstrap-buttons.component';
import {NgBootstrapAlertComponent} from './ng-bootstrap-alert/ng-bootstrap-alert.component';
import {NgBootstrapCollapseComponent} from './ng-bootstrap-collapse/ng-bootstrap-collapse.component';
import {NgBootstrapDropwdownComponent} from './ng-bootstrap-dropwdown/ng-bootstrap-dropwdown.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  declarations: [NgBootstrapMainComponent, NgBootstrapButtonsComponent, NgBootstrapAlertComponent, NgBootstrapCollapseComponent, NgBootstrapDropwdownComponent],
  imports: [
    SharedModule,
    NgBootstrapRoutingModule,
    NgbModule,
    // NgbAlertModule,
    // NgbButtonsModule,
    // NgbCollapseModule
  ]
})
export class NgBootstrapModule {
}
