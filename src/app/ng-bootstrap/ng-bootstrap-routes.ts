import {Routes} from '@angular/router';
import {NgBootstrapMainComponent} from './ng-bootstrap-main/ng-bootstrap-main.component';
import {NgBootstrapButtonsComponent} from './ng-bootstrap-buttons/ng-bootstrap-buttons.component';
import {NgBootstrapAlertComponent} from './ng-bootstrap-alert/ng-bootstrap-alert.component';
import {NgBootstrapCollapseComponent} from './ng-bootstrap-collapse/ng-bootstrap-collapse.component';
import {NgBootstrapDropwdownComponent} from './ng-bootstrap-dropwdown/ng-bootstrap-dropwdown.component';

export const NG_BOOTSTRAP_ROUTES_CONFIG: Routes = [
  {
    path: '',
    component: NgBootstrapMainComponent,
    children: [
      {
        path: 'buttons',
        component: NgBootstrapButtonsComponent
      },
      {
        path: 'alerts',
        component: NgBootstrapAlertComponent
      },
      {
        path: 'collapse',
        component: NgBootstrapCollapseComponent
      },
      {
        path: 'dropdown',
        component: NgBootstrapDropwdownComponent
      }
    ]
  }
];
