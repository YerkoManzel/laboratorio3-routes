import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-ng-bootstrap-buttons',
  templateUrl: './ng-bootstrap-buttons.component.html',
  styleUrls: ['./ng-bootstrap-buttons.component.scss']
})
export class NgBootstrapButtonsComponent implements OnInit {

  public modelCheck = {
    left: true,
    middle: false,
    right: false
  };

  public modelRadio = 1;

  constructor() {
  }

  ngOnInit() {
  }
}
