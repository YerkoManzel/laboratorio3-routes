import {Component, OnInit} from '@angular/core';
import {NGB_ALERTS, NgBootstrapAlert} from './ng-bootstrap-alert';

@Component({
  selector: 'app-ng-bootstrap-alert',
  templateUrl: './ng-bootstrap-alert.component.html',
  styleUrls: ['./ng-bootstrap-alert.component.scss']
})
export class NgBootstrapAlertComponent implements OnInit {

  public alerts: NgBootstrapAlert[];

  constructor() {
    this.reset();
  }

  ngOnInit() {
  }

  public close(alert: NgBootstrapAlert): void {
    this.alerts.splice(this.alerts.indexOf(alert), 1);
  }

  public reset(): void {
    this.alerts = Array.from(NGB_ALERTS);
  }
}
