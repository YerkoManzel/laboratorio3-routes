export interface NgBootstrapAlert {
  type: string;
  message: string;
}

export const NGB_ALERTS: NgBootstrapAlert[] = [
  {
    type: 'success',
    message: 'This is a success alert'
  },
  {
    type: 'info',
    message: 'This is a info alert'
  },
  {
    type: 'warning',
    message: 'This is a warning alert'
  },
  {
    type: 'danger',
    message: 'This is a danger alert'
  },
  {
    type: 'primary',
    message: 'This is a primary alert'
  },
  {
    type: 'secondary',
    message: 'This is a secondary alert'
  },
  {
    type: 'light',
    message: 'This is a light alert'
  },
  {
    type: 'dark',
    message: 'This is a dark alert'
  }
];
