import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgBootstrapCollapseComponent } from './ng-bootstrap-collapse.component';

describe('NgBootstrapCollapseComponent', () => {
  let component: NgBootstrapCollapseComponent;
  let fixture: ComponentFixture<NgBootstrapCollapseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgBootstrapCollapseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgBootstrapCollapseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
