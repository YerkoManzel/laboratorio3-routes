import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-ng-bootstrap-collapse',
  templateUrl: './ng-bootstrap-collapse.component.html',
  styleUrls: ['./ng-bootstrap-collapse.component.scss']
})
export class NgBootstrapCollapseComponent implements OnInit {

  public isCollapsed: boolean;

  constructor() {
    this.isCollapsed = false;
  }

  ngOnInit() {
  }

}
