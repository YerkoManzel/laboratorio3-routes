import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimengInputComponent } from './primeng-input.component';

describe('PrimengInputComponent', () => {
  let component: PrimengInputComponent;
  let fixture: ComponentFixture<PrimengInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimengInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimengInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
