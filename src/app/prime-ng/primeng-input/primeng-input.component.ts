import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-primeng-input',
  templateUrl: './primeng-input.component.html',
  styleUrls: ['./primeng-input.component.scss']
})
export class PrimengInputComponent implements OnInit {

  public sliderValues: {
    val1?: number,
    val2?: number,
    val3?: number,
    val4?: number,
    val5?: number,
    rangeValues?: number[]
  };

  public ratingValues: {
    val1?: number,
    val2?: number
  };

  public ratingMessage: string;

  public maskValues: {
    val1?: string,
    val2?: string
  };


  constructor() {
    this.sliderValues = {
      val2: 50,
      rangeValues: [20, 80]
    };

    this.ratingValues = {
      val1: 5
    };

    this.maskValues = {};
  }

  ngOnInit() {
  }

  public handleRate(event): void {
    this.ratingMessage = 'You have rated ' + event.value;
  }

  public handleCancelRate(event): void {
    this.ratingMessage = 'Rating Cancelled';
  }

}
