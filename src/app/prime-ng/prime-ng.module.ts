import {NgModule} from '@angular/core';

import {PrimeNGRoutingModule} from './prime-ng-routing.module';
import {PrimeNgMainComponent} from './prime-ng-main/prime-ng-main.component';
import {PrimeNgDataComponent} from './prime-ng-data/prime-ng-data.component';
import {PrimeNgListaComponent} from './prime-ng-lista/prime-ng-lista.component';
import {PrimeNgDetalleComponent} from './prime-ng-detalle/prime-ng-detalle.component';
import {PrimengInputComponent} from './primeng-input/primeng-input.component';
import {SliderModule} from 'primeng/slider';
import {SharedModule} from '../shared/shared.module';
import {RatingModule} from 'primeng/rating';
import {AccordionModule, ButtonModule, CardModule, GalleriaModule, InputMaskModule} from 'primeng/primeng';
import {PrimengButtonComponent} from './primeng-button/primeng-button.component';
import {PrimengPanelComponent} from './primeng-panel/primeng-panel.component';
import {PrimengMessagesComponent} from './primeng-messages/primeng-messages.component';
import {ToastModule} from 'primeng/toast';
import {PrimengGalleriaComponent} from './primeng-galleria/primeng-galleria.component';

@NgModule({
  declarations: [
    PrimeNgMainComponent,
    PrimeNgDataComponent,
    PrimeNgListaComponent,
    PrimeNgDetalleComponent,
    PrimengInputComponent,
    PrimengButtonComponent,
    PrimengPanelComponent,
    PrimengMessagesComponent,
    PrimengGalleriaComponent
  ],
  imports: [
    SharedModule,
    PrimeNGRoutingModule,
    SliderModule,
    RatingModule,
    InputMaskModule,
    ButtonModule,
    AccordionModule,
    CardModule,
    ToastModule,
    GalleriaModule
  ]
})
export class PrimeNGModule {
}
