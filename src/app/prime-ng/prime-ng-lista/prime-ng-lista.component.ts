import {Component, OnInit} from '@angular/core';
import {ListService} from './list.service';

@Component({
  selector: 'app-prime-ng-lista',
  templateUrl: './prime-ng-lista.component.html',
  styleUrls: ['./prime-ng-lista.component.scss']
})
export class PrimeNgListaComponent implements OnInit {

  public details: { id: number, detail: string }[];

  constructor(private listService: ListService) {
    this.details = [];
  }

  ngOnInit() {
    this.details = this.listService.detail;
  }

}
