import {Component, OnInit} from '@angular/core';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-primeng-messages',
  templateUrl: './primeng-messages.component.html',
  styleUrls: ['./primeng-messages.component.scss'],
  providers: [MessageService]
})
export class PrimengMessagesComponent implements OnInit {

  constructor(private messageService: MessageService) {
  }

  ngOnInit() {
  }

  public showSuccess(): void {
    this.messageService.add({severity: 'success', summary: 'Success Message', detail: 'Order submitted'});
  }

  public showInfo(): void {
    this.messageService.add({severity: 'info', summary: 'Info Message', detail: 'PrimeNG rocks'});
  }

  public showWarn(): void {
    this.messageService.add({severity: 'warn', summary: 'Warn Message', detail: 'There are unsaved changes'});
  }

  public showError(): void {
    this.messageService.add({severity: 'error', summary: 'Error Message', detail: 'Validation failed'});
  }

  public showCustom(): void {
    this.messageService.add({key: 'custom', severity: 'info', summary: 'Custom Toast', detail: 'Witha Gradient', life: 10000});
  }

  public showTopLeft(): void {
    this.messageService.add({key: 'tl', severity: 'info', summary: 'Success Message', detail: 'Order submitted'});
  }

  public showTopCenter(): void {
    this.messageService.add({key: 'tc', severity: 'warn', summary: 'Info Message', detail: 'PrimeNG rocks'});
  }

  public showConfirm(): void {
    this.messageService.clear();
    this.messageService.add({key: 'c', sticky: true, severity: 'warn', summary: 'Are you sure?', detail: 'Confirm to proceed'});
  }

  public showMultiple(): void {
    this.messageService.addAll([{severity: 'info', summary: 'Message 1', detail: 'PrimeNG rocks'}, {
      severity: 'info',
      summary: 'Message 2',
      detail: 'PrimeUI rocks'
    }, {severity: 'info', summary: 'Message 3', detail: 'PrimeFaces rocks'}]);
  }

  public onConfirm(): void {
    this.messageService.clear('c');
  }

  public onReject(): void {
    this.messageService.clear('c');
  }

  public clear(): void {
    this.messageService.clear();
  }

}
