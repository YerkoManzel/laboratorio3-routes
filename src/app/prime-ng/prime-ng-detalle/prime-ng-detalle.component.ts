import {Component, OnInit} from '@angular/core';
import {ListService} from '../prime-ng-lista/list.service';
import {ActivatedRoute, ParamMap} from '@angular/router';

@Component({
  selector: 'app-prime-ng-detalle',
  templateUrl: './prime-ng-detalle.component.html',
  styleUrls: ['./prime-ng-detalle.component.scss']
})
export class PrimeNgDetalleComponent implements OnInit {

  public detail: { id: number, detail: string };

  constructor(private listService: ListService,
              private _activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe((params: ParamMap) => {
      let id = params.get('id');
      this.detail = this.listService.getById(+id);
    });
  }

}
