import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-primeng-button',
  templateUrl: './primeng-button.component.html',
  styleUrls: ['./primeng-button.component.scss']
})
export class PrimengButtonComponent implements OnInit {

  public clicks: number;

  constructor() {
    this.clicks = 0;
  }

  ngOnInit() {
  }

  public count(): void {
    this.clicks++;
  }

}
