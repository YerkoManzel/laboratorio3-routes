import {Routes} from '@angular/router';
import {PrimeNgMainComponent} from './prime-ng-main/prime-ng-main.component';
import {PrimeNgDataComponent} from './prime-ng-data/prime-ng-data.component';
import {PrimeNgListaComponent} from './prime-ng-lista/prime-ng-lista.component';
import {PrimeNgDetalleComponent} from './prime-ng-detalle/prime-ng-detalle.component';
import {PrimengInputComponent} from './primeng-input/primeng-input.component';
import {PrimengButtonComponent} from './primeng-button/primeng-button.component';
import {PrimengPanelComponent} from './primeng-panel/primeng-panel.component';
import {PrimengMessagesComponent} from './primeng-messages/primeng-messages.component';
import {PrimengGalleriaComponent} from './primeng-galleria/primeng-galleria.component';

export const PRIME_NG_ROUTES_CONFIG: Routes = [
  {
    path: '',
    component: PrimeNgMainComponent,
    children: [
      {
        path: 'data',
        component: PrimeNgDataComponent
      },
      {
        path: 'lista',
        component: PrimeNgListaComponent,
        children: [
          {
            path: ':id',
            component: PrimeNgDetalleComponent
          }
        ]
      },
      {
        path: 'input',
        component: PrimengInputComponent
      },
      {
        path: 'button',
        component: PrimengButtonComponent
      },
      {
        path: 'panel',
        component: PrimengPanelComponent
      },
      {
        path: 'message',
        component: PrimengMessagesComponent
      },
      {
        path: 'galleria',
        component: PrimengGalleriaComponent
      },
      {
        path: '**',
        redirectTo: '/primeng/data',
        component: PrimeNgDataComponent
      }
    ]
  }
];
